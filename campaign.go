package patreon

type Campaign struct {
	ID      string    `jsonapi:"primary,campaign"`
	Creator *User     `jsonapi:"relation,creator"`
	Goals   []*Goal   `jsonapi:"relation,goals"`
	Rewards []*Reward `jsonapi:"relation,rewwards"`
}
