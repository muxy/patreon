package patreon

import "fmt"
import "github.com/google/jsonapi"

type User struct {
	ID       string `jsonapi:"primary,user"`
	FullName string `jsonapi:"attr,full_name"`
	//Campaign *Campaign `jsonapi:"relation,campaign"`
}

func (client *PatreonClient) GetCurrentUser() (*User, error) {
	var user User

	resp, err := client.client.Get("https://api.patreon.com/oauth2/api/current_user")
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != 200 {
		return nil, fmt.Errorf("Status: %d", resp.StatusCode)
	}
	defer resp.Body.Close()

	if err := jsonapi.UnmarshalPayload(resp.Body, &user); err != nil {
		return nil, err
	}

	return &user, nil
}
