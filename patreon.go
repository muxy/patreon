package patreon

import "net/http"

type PatreonClient struct {
	client *http.Client
}

func NewPatreonClient(httpClient *http.Client) PatreonClient {
	return PatreonClient{
		client: httpClient,
	}
}
