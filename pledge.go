package patreon

type Pledge struct {
	ID          string    `jsonapi:"primary,pledge"`
	AmountCents int64     `jsonapi:"attr,amount_cents"`
	Campaign    *Campaign `jsonapi:"relation,campaign"`
	Patron      *User     `jsonapi:"relation,patron"`
}
