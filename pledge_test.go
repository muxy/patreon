package patreon_test

import (
	"log"
	"strings"
	"testing"

	"bitbucket.org/muxy/patreon"
	"github.com/google/jsonapi"
)

func TestUnMarshalPledge(t *testing.T) {
	pledge := patreon.Pledge{}

	r := strings.NewReader(pledgeTestJSON)

	if err := jsonapi.UnmarshalPayload(r, &pledge); err != nil {
		t.Error(err)
		return
	}

	log.Printf("%+v", pledge.Campaign.Creator)
}

var pledgeTestJSON = `
{
    "data": {
        "attributes": {
            "amount_cents": 6000,
            "created_at": "2017-06-01T20:32:59.217724+00:00",
            "declined_since": null,
            "is_twitch_pledge": false,
            "patron_pays_fees": false,
            "pledge_cap_cents": 6000
        },
        "id": "6131804",
        "relationships": {
            "address": {
                "data": {
                    "id": "558145",
                    "type": "address"
                }
            },
            "campaign": {
                "data": {
                    "id": "380313",
                    "type": "campaign"
                },
                "links": {
                    "related": "https://api.patreon.com/campaigns/380313"
                }
            },
            "patron": {
                "data": {
                    "id": "2741722",
                    "type": "user"
                },
                "links": {
                    "related": "https://api.patreon.com/user/2741722"
                }
            },
            "reward": {
                "data": {
                    "id": "1730706",
                    "type": "reward"
                },
                "links": {
                    "related": "https://api.patreon.com/rewards/1730706"
                }
            }
        },
        "type": "pledge"
    },
    "included": [
        {
            "attributes": {
                "about": "",
                "created": "2016-01-15T05:02:54+00:00",
                "email": "alfredoamares@gmail.com",
                "facebook": null,
                "first_name": "Alfredo",
                "full_name": "Alfredo amarales",
                "gender": 0,
                "image_url": "https://c3.patreon.com/2/patreon-user/ljjILrqTSCFMsoFQ6kZ8vVpz19KWs8QHRgo3J7T846AJlLIGHUTinejXL21zgoo6_large_2.jpeg?t=2145916800&w=400&v=y-NaRSrcMrrLYWh6BPa3KlDXI6dZYO7fWl_smH9xcgE%3D",
                "is_email_verified": false,
                "last_name": "amarales",
                "social_connections": {
                    "deviantart": null,
                    "discord": {
                        "user_id": "309618436621991938"
                    },
                    "facebook": null,
                    "spotify": null,
                    "twitch": null,
                    "twitter": null,
                    "youtube": null
                },
                "thumb_url": "https://c3.patreon.com/2/patreon-user/ljjILrqTSCFMsoFQ6kZ8vVpz19KWs8QHRgo3J7T846AJlLIGHUTinejXL21zgoo6_large_2.jpeg?h=100&t=2145916800&w=100&v=ETrM27pGNtu_GUC3T7iqcp9k95tFkfKrFLvQ4FJnC94%3D",
                "twitch": null,
                "twitter": "Sc: oderfla1990",
                "url": "https://www.patreon.com/user?u=2741722",
                "vanity": null,
                "youtube": null
            },
            "id": "2741722",
            "relationships": {
                "campaign": {
                    "data": null
                }
            },
            "type": "user"
        },
        {
            "attributes": {
                "amount": 6000,
                "amount_cents": 6000,
                "created_at": "2017-05-31T12:24:04.750881+00:00",
                "deleted_at": null,
                "description": "\u00a0You've gained a Magical god sword , use this wisely, hero. <br>\nWith this tier you unlock everything from all lower tiers, as well as these cool new perks; <br>\n- A Personalised thank you video after your first pledge! <br>\n- Access to all archived pics from previous months plus more content for LVL5 eyes only! <br>\n- A care package will be sent twice per year full of awesome goodies if your pledge is consistent. First will be sent on the 6th month.<br>\n - Help me decide what to wear for shoots and what stuff to buy! <br>\n- Free new A4 cosplay prints every time I do a new shoot!<br>\n- Personal pics/fan signs!\u00a0",
                "discord_role_ids": [
                    "215567106987524098",
                    "187611570191335424",
                    "285877675694882836"
                ],
                "edited_at": "2017-05-31T12:26:40.023719+00:00",
                "image_url": "https://s3-us-west-1.amazonaws.com/patreon-reward-images/1730706.png",
                "is_twitch_reward": false,
                "patron_count": 5,
                "post_count": 35,
                "published": true,
                "published_at": "2017-05-31T12:26:40.014658+00:00",
                "remaining": null,
                "requires_shipping": true,
                "title": "Lvl 5",
                "unpublished_at": null,
                "url": "/bePatron?c=380313&rid=1730706",
                "user_limit": null
            },
            "id": "1730706",
            "relationships": {
                "campaign": {
                    "data": {
                        "id": "380313",
                        "type": "campaign"
                    },
                    "links": {
                        "related": "https://api.patreon.com/campaigns/380313"
                    }
                },
                "creator": {
                    "data": {
                        "id": "2361264",
                        "type": "user"
                    },
                    "links": {
                        "related": "https://api.patreon.com/user/2361264"
                    }
                }
            },
            "type": "reward"
        },
        {
            "attributes": {
                "created_at": "2016-06-01T10:54:41+00:00",
                "creation_count": 110,
                "creation_name": "Live Streams, YouTube Videos & Cosplay",
                "discord_server_id": "187611077717000193",
                "display_patron_goals": false,
                "earnings_visibility": "private",
                "image_small_url": "https://c3.patreon.com/2/patreon-user/gsM5OR4x6iUiuGFXfrWtRfEtVMI9lzu8pUE1lhEGCSt3o7meI3QMMQy0PjXiDM9E.png?h=1280&t=1497484800&w=1280&v=9_LHxdWxSZ2JrcTyk6rTG0gmV_ehL5G9rvInKeQx7tM%3D",
                "image_url": "https://c3.patreon.com/2/patreon-user/gsM5OR4x6iUiuGFXfrWtRfEtVMI9lzu8pUE1lhEGCSt3o7meI3QMMQy0PjXiDM9E.png?t=1497484800&w=1920&v=60m5EaYPgwcTAmR-PtjzaGYbhNWj_z_Hf8EkdJIwNsg%3D",
                "is_charged_immediately": true,
                "is_monthly": true,
                "is_nsfw": true,
                "is_plural": false,
                "main_video_embed": "",
                "main_video_url": "",
                "one_liner": null,
                "outstanding_payment_amount_cents": 0,
                "patron_count": 30,
                "pay_per_name": "month",
                "pledge_sum": 79836,
                "pledge_url": "/bePatron?c=380313",
                "published_at": "2016-12-02T13:41:37+00:00",
                "summary": "<h4 style=\"text-align: center;\"><strong style=\"background-color: initial;\">Hey guys, first I'd like to thank you for visiting my Patreon!</strong></h4><h4 style=\"text-align: center;\"><strong style=\"background-color: initial;\">Welcome!\u00a0</strong></h4><div style=\"text-align: center;\">\n<span style=\"color: rgb(63, 63, 63);\"><span style=\"color: rgb(38, 38, 38);\">My name is Kerri, but I am also known as riiri, I do things online and sometimes make some things and build some stuff.\n\t<br>\nI've decided to create a Patreon surrounding my love for streaming, cosplaying and sharing lewd things! I've been streaming on and off for maybe 3 years now but only the past 1-2 years I've started to fully dive into it. As for cosplay, I think it's been around 3 years also!<br>My streaming currently consists of 4 days (sometimes 5) per week; Tuesday, Wednesday, Friday &amp; Saturday. I stream games but I've dabbled with PC building on stream and really loved that too, so in the future I'd love to do more creative streams in hopes of teaching new or interested minds about building their own. I'd also love to get into Cosplay making streams once I have the room!<br></span><em><span style=\"color: rgb(38, 38, 38);\">Just a quick reminder, you're never obligated to pledge, this patreon is <strong>purely</strong> for people who wish to help me further!</span></em><br></span><br><img src=\"http://i.imgur.com/g1f1xxD.jpg\"><br></div><h3><span style=\"color: rgb(192, 80, 77);\"><span style=\"color: rgb(75, 172, 198);\"><span style=\"color: rgb(75, 172, 198);\"><span style=\"color: rgb(75, 172, 198);\">What can you expect?</span></span></span></span></h3><span style=\"color: rgb(0, 0, 0);\"><span style=\"color: rgb(38, 38, 38);\"><strong>- Access to Patreon-only feed, Patreon-only Discord Chat and exclusive Discount!</strong><br>This will include posts, polls and discussions! For discord you will get a Special Patreon role and access to special chat rooms! You will also get discount for my print store (higher the role the more discount you get) and any future merch!<br><strong>- Personalised polaroids and Special Event cards!</strong><br>For LVL 4 and LVL 5 Patreons you can receive NSFW polaroids as well as derpy/selfies! Again, depending on your level you will receive more or less cards for special occasions.<br><strong>- Cosplay Content!</strong><br>Be the first ones to see my new cosplay! You will also get exclusive HD content such as; time lapse videos, photo guides, photo updates and cosplay test photos. Depending on your level you will receive free cosplay prints!<br><strong>- Lewd Content (18+ ONLY)</strong><br>Gain access to numerous ecchi pictures and videos! I can be your new online 3D anime waifu and keep your thirst quenched with lots of booty, underwear and smoothness... \u00a0<br><strong>- Help me decide what to shoot!</strong><br>Depending on your level you will be able to help me decide what to shoot, give your say and help me pick cosplays and \"garments\" that you'd like to see!<br><strong>- Behind the Scene Content!</strong><br>Depending on your level you will get BtS content from all professional shoots I do! This includes Cosplay, Casual and Boudoir!<br><strong>- Voice/Video Chats for LVL 5 Patreons!</strong><br>Every month we will hold a voice or video call, depending on the platform of choice, where we can all chill and chat about things! Ask me anything or just hang out and play games with me!<br><strong>- LVL 5 Patreon Care packages!</strong><br>After 6 months of pledging as a LVL 5 patreon, you will receive a bountiful care package with items hand picked by myself!</span><br><strong>Plus lots more!<br><br></strong></span><h3><span style=\"color: rgb(0, 0, 0);\"><strong><span style=\"color: rgb(128, 100, 162);\">How does this work?</span></strong></span></h3><span style=\"color: rgb(0, 0, 0);\"><strong></strong><span style=\"color: rgb(38, 38, 38);\">Upon pledging you will pay your desired tier amount, be careful as the reset payment happens on the 1st of every month so unless you wish to see last months lewd content, try to avoid pledging at the end of the month! Throughout the month I will upload content as described!<br>At the end of the month I will send out any rewards, such as welcome cards, thank you videos, care packages, etc.<br>For the LVL 4 lewd content everything will be bundled into a dropbox folder where you have 30 days to save anything (if you wish). After those 30 days the link will expire. Any LVL 4 lewd content will be changed to LVL 5 for archiving reasons, so be sure to save before the month ends or save from the drop box folder!<br>For the LVL 5 lewd content there is a mass archiving drop box where you have access to all posts (the link refreshes every 30 days also) but you will also be able to view them on patreon too!<br><br><em>Please remember to not share any patreon only content with others, it's for personal private use only</em> ^_^</span></span><span style=\"color: rgb(0, 0, 0);\"><br><img src=\"https://pbs.twimg.com/media/C9i-o4-XUAAVeLk.jpg:large\"><br></span><div style=\"text-align: center;\">I<span style=\"color: rgb(38, 38, 38);\">mage from i60 - Photo by ASUS ROG UK\u00a0</span></div><span style=\"color: rgb(38, 38, 38);\"><span style=\"color: rgb(0, 0, 0);\"><span style=\"color: rgb(192, 80, 77);\"><span style=\"color: rgb(192, 80, 77);\">\n<br>\n</span></span></span>\n</span><div style=\"text-align: center;\">\n<em><span style=\"color: rgb(38, 38, 38);\">Plus stuff like this happens on stream when I reach follow goals, so I guess you guys can help decide what happens next time...?</span></em></div>\n<span style=\"color: rgb(192, 80, 77);\"><span style=\"color: rgb(192, 80, 77);\">\n<iframe allowfullscreen=\"\" frameborder=\"0\" height=\"315\" src=\"https://www.youtube.com/embed/UMA3ipZOChc\" width=\"560\"></iframe>\n<br>\n<div style=\"text-align: center;\">\n<span style=\"color: rgb(0, 0, 0);\"><span style=\"color: rgb(38, 38, 38);\">Got any questions regarding my Patreon or just want to have a snoop around my social media? Links are below!<br>\n<a href=\"https://twitter.com/riiri_senpai\" rel=\"nofollow\">Twitter<br>\n</a><a href=\"http://instagram.com/riirichan\" rel=\"nofollow\">Instagram<br>\n</a><a href=\"http://youtube.com/c/riirichan\" rel=\"nofollow\">YouTube<br>\n</a><a href=\"http://twitch.tv/riirichan\" rel=\"nofollow\">Twitch<br>\n</a><a href=\"https://www.facebook.com/riirichan/\" rel=\"nofollow\">Facebook<br>\n</a>Snapchat; riirichan<br>\n<a href=\"http://discord.gg/cQCQ2bw \" rel=\"nofollow\">Discord<br>\n</a>Thank you all so much again for the support! Also thank you for visiting my patreon, if you decide to join my smol family then I'm sending a huge hug your way! &lt;3<br><br><em>If you wish to purchase single content (such as NSFW images etc) from my Patreon, head over to my\u00a0<a href=\"http://extralunchmoney.com/user/pinkuxkitty\" target=\"_blank\">ELM Page</a>!</em><br>\n\t- riiri</span>\n\t</span>\n</div>\n<div style=\"text-align: center;\">\n</div>\n<div style=\"text-align: center;\">\n</div>\n</span></span>",
                "thanks_embed": "",
                "thanks_msg": "<strong>Hello my lovely new patreon! </strong><br>Thank you so so much from the bottom of my heart for joining me in my journey! I really hope I can do you proud and produce content that you will love! After revamping my Patreon I wish to keep things chill until I'm able to take a full dive into all the craziness that is monthly photo shoots and live streams 5 days per week!<br>We've already managed to pass quite a few goals thank to you all!<br>Regardless of your tier or the pledge amount, I am so beyond grateful to have you in my little army of kouhai &lt;3<br><br>If you have a spare moment please fill out the form depending on your level, just so it makes it easier for me to know who wants which rewards!<br><strong>Level 3\u00a0</strong>:\u00a0<a href=\"https://goo.gl/forms/9a7RqW3zgGSpQgIv1\">https://goo.gl/forms/9a7RqW3zgGSpQgIv1</a><br><strong>Level 4/5</strong> :\u00a0<a href=\"https://goo.gl/forms/kZp2KmcZJRhGUyxj2\">https://goo.gl/forms/kZp2KmcZJRhGUyxj2<br></a>If you <strong>do not</strong> wish to receive any postal rewards <strong>PLEASE</strong> let me know, otherwise I'll not know. Preferably in the Address section, leave it blank or fill with \"No Postal Rewards\"<br><br>I can't thank you enough for your help and I appreciate every ounce of your being!<br>I hope you have a lovely day/evening/night wherever you are!<br><br>- love from riiri\u00a0<br>xo<br><br>",
                "thanks_video_url": null
            },
            "id": "380313",
            "relationships": {
                "creator": {
                    "data": {
                        "id": "2361264",
                        "type": "user"
                    },
                    "links": {
                        "related": "https://api.patreon.com/user/2361264"
                    }
                },
                "goals": {
                    "data": [
                        {
                            "id": "628719",
                            "type": "goal"
                        },
                        {
                            "id": "632671",
                            "type": "goal"
                        },
                        {
                            "id": "632682",
                            "type": "goal"
                        },
                        {
                            "id": "662906",
                            "type": "goal"
                        },
                        {
                            "id": "662907",
                            "type": "goal"
                        },
                        {
                            "id": "738718",
                            "type": "goal"
                        }
                    ]
                },
                "rewards": {
                    "data": [
                        {
                            "id": "-1",
                            "type": "reward"
                        },
                        {
                            "id": "0",
                            "type": "reward"
                        },
                        {
                            "id": "1116258",
                            "type": "reward"
                        },
                        {
                            "id": "1116257",
                            "type": "reward"
                        },
                        {
                            "id": "1116256",
                            "type": "reward"
                        },
                        {
                            "id": "1238785",
                            "type": "reward"
                        },
                        {
                            "id": "1730706",
                            "type": "reward"
                        }
                    ]
                }
            },
            "type": "campaign"
        },
        {
            "attributes": {
                "addressee": "Alfredo Amarales",
                "city": "Simi",
                "country": "US",
                "line_1": "No mail rewards please",
                "line_2": null,
                "postal_code": "93065",
                "state": "CA"
            },
            "id": "558145",
            "type": "address"
        },
        {
            "attributes": {
                "about": "",
                "created": "2015-09-21T13:11:14+00:00",
                "discord_id": "142326408243314688",
                "email": "riiri.chan@gmail.com",
                "facebook": "https://www.facebook.com/riirichan",
                "facebook_id": "522890167880087",
                "first_name": "riiri-chan",
                "full_name": "riiri-chan",
                "gender": 0,
                "has_password": true,
                "image_url": "https://c3.patreon.com/2/patreon-user/pfoFmhXzjJk3aXZZbMDwAvcsIo0lewyyKMStbIZ7lEHZabIPzMgxhONJymU7pu7G_large_2.jpeg?t=2145916800&w=400&v=EY2S5KHf97x3wxVy7yYUZGOqamV9jtHh4U0YNw-hPa4%3D",
                "is_deleted": false,
                "is_email_verified": true,
                "is_nuked": false,
                "is_suspended": false,
                "last_name": "",
                "social_connections": {
                    "deviantart": null,
                    "discord": {
                        "scopes": [
                            "identify",
                            "guilds.join",
                            "guilds"
                        ],
                        "user_id": "142326408243314688"
                    },
                    "facebook": null,
                    "spotify": null,
                    "twitch": null,
                    "twitter": {
                        "user_id": "2996894176"
                    },
                    "youtube": null
                },
                "thumb_url": "https://c3.patreon.com/2/patreon-user/pfoFmhXzjJk3aXZZbMDwAvcsIo0lewyyKMStbIZ7lEHZabIPzMgxhONJymU7pu7G_large_2.jpeg?h=100&t=2145916800&w=100&v=2cb-vnetR77_ADm0Jr8vXNlFBoBs0beHZ-dKE3ayvvs%3D",
                "twitch": "http://www.Twitch.tv/riirichan",
                "twitter": "Riiri_senpai",
                "url": "https://www.patreon.com/riirichan",
                "vanity": "riirichan",
                "youtube": "https://www.youtube.com/c/riirichan "
            },
            "id": "2361264",
            "relationships": {
                "campaign": {
                    "data": {
                        "id": "380313",
                        "type": "campaign"
                    },
                    "links": {
                        "related": "https://api.patreon.com/campaigns/380313"
                    }
                }
            },
            "type": "user"
        },
        {
            "attributes": {
                "amount": 0,
                "amount_cents": 0,
                "created_at": null,
                "description": "Everyone",
                "remaining": 0,
                "requires_shipping": false,
                "type": "reward",
                "url": null,
                "user_limit": null
            },
            "id": "-1",
            "relationships": {
                "creator": {
                    "data": {
                        "id": "2361264",
                        "type": "user"
                    },
                    "links": {
                        "related": "https://api.patreon.com/user/2361264"
                    }
                }
            },
            "type": "reward"
        },
        {
            "attributes": {
                "amount": 1,
                "amount_cents": 1,
                "created_at": null,
                "description": "Patrons Only",
                "remaining": 0,
                "requires_shipping": false,
                "type": "reward",
                "url": null,
                "user_limit": null
            },
            "id": "0",
            "relationships": {
                "creator": {
                    "data": {
                        "id": "2361264",
                        "type": "user"
                    },
                    "links": {
                        "related": "https://api.patreon.com/user/2361264"
                    }
                }
            },
            "type": "reward"
        },
        {
            "attributes": {
                "amount": 100,
                "amount_cents": 100,
                "created_at": "2016-12-02T14:08:36+00:00",
                "deleted_at": null,
                "description": "Welcome beginner...<br>\n<br>\nThank you so much for supporting me! This tier is for people who just wish to help out, making me $1 closer to my goals!<br>\n",
                "discord_role_ids": null,
                "edited_at": "2017-04-06T22:47:16.644874+00:00",
                "image_url": "https://s3-us-west-1.amazonaws.com/patreon-reward-images/1116258.jpeg",
                "is_twitch_reward": false,
                "patron_count": 2,
                "post_count": 13,
                "published": true,
                "published_at": "2016-12-02T14:08:36+00:00",
                "remaining": null,
                "requires_shipping": false,
                "title": "Lvl 1",
                "unpublished_at": null,
                "url": "/bePatron?c=380313&rid=1116258",
                "user_limit": null
            },
            "id": "1116258",
            "relationships": {
                "campaign": {
                    "data": {
                        "id": "380313",
                        "type": "campaign"
                    },
                    "links": {
                        "related": "https://api.patreon.com/campaigns/380313"
                    }
                },
                "creator": {
                    "data": {
                        "id": "2361264",
                        "type": "user"
                    },
                    "links": {
                        "related": "https://api.patreon.com/user/2361264"
                    }
                }
            },
            "type": "reward"
        },
        {
            "attributes": {
                "amount": 500,
                "amount_cents": 500,
                "created_at": "2016-12-02T14:08:36+00:00",
                "deleted_at": null,
                "description": "Woah you just levelled up!<br>\n<br>\nWith this tier you unlock lots of cool rewards and perks such as;<br>\n- Access to Patreon-only feed<br>\n- Access to Patreon-only Discord chat and voice room on my Discord Server<br>\n- Patreon-only Lvl 2 polls<br>\n<strong>- and more cool things to come!</strong><br>\n",
                "discord_role_ids": [
                    "187611570191335424"
                ],
                "edited_at": "2017-04-06T22:47:58.285683+00:00",
                "image_url": "https://s3-us-west-1.amazonaws.com/patreon-reward-images/1116257.jpeg",
                "is_twitch_reward": false,
                "patron_count": 6,
                "post_count": 8,
                "published": true,
                "published_at": "2016-12-02T14:08:36+00:00",
                "remaining": null,
                "requires_shipping": false,
                "title": "Lvl 2",
                "unpublished_at": null,
                "url": "/bePatron?c=380313&rid=1116257",
                "user_limit": null
            },
            "id": "1116257",
            "relationships": {
                "campaign": {
                    "data": {
                        "id": "380313",
                        "type": "campaign"
                    },
                    "links": {
                        "related": "https://api.patreon.com/campaigns/380313"
                    }
                },
                "creator": {
                    "data": {
                        "id": "2361264",
                        "type": "user"
                    },
                    "links": {
                        "related": "https://api.patreon.com/user/2361264"
                    }
                }
            },
            "type": "reward"
        },
        {
            "attributes": {
                "amount": 1500,
                "amount_cents": 1500,
                "created_at": "2016-12-02T14:08:36+00:00",
                "deleted_at": null,
                "description": "Soon you'll be able to defeat the boss! <strong>Jokes</strong>, here's a wooden sword!<br>\n<br>\nWith this tier you unlock everything from Lvl 1. &amp; Lvl 2. as well as these cool new perks;<br>\n- Receive a welcoming 'thank you' card with a derpy polaroid within 1 month of your first pledge.\n<p>- Cosplay updates<br>\n- Christmas &amp; Valentine's Day cards<em> </em>that will include polaroids!<br>\n- Lvl 3 Patreon-only polls.<br>\n- Print &amp; future merch discount.<br>\n- Priority invites to games during and off streams!<br>\n<strong>- and more cool things to come!</strong></p>",
                "discord_role_ids": [
                    "187611570191335424"
                ],
                "edited_at": "2017-04-09T16:32:44.974939+00:00",
                "image_url": "https://s3-us-west-1.amazonaws.com/patreon-reward-images/1116256.jpeg",
                "is_twitch_reward": false,
                "patron_count": 3,
                "post_count": 13,
                "published": true,
                "published_at": "2016-12-02T14:08:36+00:00",
                "remaining": null,
                "requires_shipping": true,
                "title": "Lvl 3",
                "unpublished_at": null,
                "url": "/bePatron?c=380313&rid=1116256",
                "user_limit": null
            },
            "id": "1116256",
            "relationships": {
                "campaign": {
                    "data": {
                        "id": "380313",
                        "type": "campaign"
                    },
                    "links": {
                        "related": "https://api.patreon.com/campaigns/380313"
                    }
                },
                "creator": {
                    "data": {
                        "id": "2361264",
                        "type": "user"
                    },
                    "links": {
                        "related": "https://api.patreon.com/user/2361264"
                    }
                }
            },
            "type": "reward"
        },
        {
            "attributes": {
                "amount": 3000,
                "amount_cents": 3000,
                "created_at": "2017-01-03T12:43:50.030657+00:00",
                "deleted_at": null,
                "description": "Your wooden sword has been upgraded to a Crystal sword! <em>Woah</em>, <strong>GO YOU</strong>!<br>\n<br>\nWith this tier you unlock everything from Lvl 1., Lvl 2. and Lvl 3 as well as these cool new perks;<br>\n- Access to Patreon-only cosplay progress and cosplay tests! This can be time-lapse videos, step by step pictures or tutorials.<br>\n- Access to Patreon-only NSFW/18+ content each month.\n<p>- Behind the scenes footage from all future shoots; videos, selfies or extra pictures. If it's casual, cosplay or boudior, you'll get BTS goodies!<br>\n- Cute Peach Polaroids if specified, if not just normal derpy polaroids<br>\n- Lvl 4 Patreon-only polls.\u00a0</p>\n<p>- Your name will be placed in a special thank you section on my Twitch channel, website, youtube videos and more!</p>\n<p><br>\n<strong>- and more cool things to come!</strong></p>",
                "discord_role_ids": [
                    "187611570191335424",
                    "215567106987524098"
                ],
                "edited_at": "2017-05-31T12:28:12.665560+00:00",
                "image_url": "https://s3-us-west-1.amazonaws.com/patreon-reward-images/1238785.jpeg",
                "is_twitch_reward": false,
                "patron_count": 17,
                "post_count": 19,
                "published": true,
                "published_at": "2017-01-03T12:43:50.030657+00:00",
                "remaining": null,
                "requires_shipping": true,
                "title": "Lvl 4",
                "unpublished_at": null,
                "url": "/bePatron?c=380313&rid=1238785",
                "user_limit": null
            },
            "id": "1238785",
            "relationships": {
                "campaign": {
                    "data": {
                        "id": "380313",
                        "type": "campaign"
                    },
                    "links": {
                        "related": "https://api.patreon.com/campaigns/380313"
                    }
                },
                "creator": {
                    "data": {
                        "id": "2361264",
                        "type": "user"
                    },
                    "links": {
                        "related": "https://api.patreon.com/user/2361264"
                    }
                }
            },
            "type": "reward"
        },
        {
            "attributes": {
                "amount": 15000,
                "amount_cents": 15000,
                "completed_percentage": 100,
                "created_at": "2016-12-02T16:51:54+00:00",
                "description": "<strong>First Goal!</strong><br>I'll collect some goodies to giveaway for you guys during a stream! Also possibly a drunk battle amongst anyone who wishes to join on stream? Who knows?",
                "reached_at": "2016-12-03T13:28:02+00:00",
                "title": ""
            },
            "id": "628719",
            "type": "goal"
        },
        {
            "attributes": {
                "amount": 25000,
                "amount_cents": 25000,
                "completed_percentage": 100,
                "created_at": "2016-12-06T16:06:36+00:00",
                "description": "<strong>Gaming Gatherings!</strong><br>At this goal I'd like to set up some gatherings for my Patreons in the UK which will be based at one of the Loading Bars in London. I'm looking to make this once every 3 months, so if people can't make it to one, there will still be 2 other events they might be able to reach! I've contacted the Loading Bar and they've given me the go ahead to hold these events which is awesome! I'm planning on packing goodie bags for my patreon peas who attend also! &lt;3",
                "reached_at": "2017-01-03T13:31:59+00:00",
                "title": ""
            },
            "id": "632671",
            "type": "goal"
        },
        {
            "attributes": {
                "amount": 50000,
                "amount_cents": 50000,
                "completed_percentage": 100,
                "created_at": "2016-12-06T16:16:44+00:00",
                "description": "<strong>More Weekly Streams &amp; Video Content!</strong><br>This goal will enable me to make more content per week, streaming more days and being able to do some YouTube content too! I'd also like to do some 12 hours streams where there will be cool prizes to give out to my viewers!",
                "reached_at": "2017-01-03T22:36:45+00:00",
                "title": ""
            },
            "id": "632682",
            "type": "goal"
        },
        {
            "attributes": {
                "amount": 120000,
                "amount_cents": 120000,
                "completed_percentage": 66,
                "created_at": "2017-01-03T09:30:50+00:00",
                "description": "<strong>Streaming Full Time &amp; New Cosplay!</strong><br>This will allow me to stream 4 set days per week on a schedule as well as a 5th flexi day! As well as just streaming, I'll be able to produce new cosplay, which means creative streams, more YT videos AND prints!",
                "reached_at": "2017-03-27T12:56:51+00:00",
                "title": ""
            },
            "id": "662906",
            "type": "goal"
        },
        {
            "attributes": {
                "amount": 250000,
                "amount_cents": 250000,
                "completed_percentage": 31,
                "created_at": "2017-01-03T09:30:50+00:00",
                "description": "<strong>Boudoir Shoots &amp; Even More Goodies!</strong><br>At this goal I'd love to say a huge thanks to anyone and everyone whom has supported me. Not only will I be able to stream full time, I'll be able to cosplay more, attend more events and make awesome boudoir shoots for all of you lovely people out there! These can be normal Kerri boudoir shoots or cosplay boudoir shoots! We'll be able to change it up monthly and provide more prints which will be super exciting!\u00a0",
                "reached_at": null,
                "title": ""
            },
            "id": "662907",
            "type": "goal"
        },
        {
            "attributes": {
                "amount": 200000,
                "amount_cents": 200000,
                "completed_percentage": 39,
                "created_at": "2017-03-05T21:03:18+00:00",
                "description": "<strong>Streaming Upgrade Revamp &amp; More outfits!</strong><br>This will allow me to revamp my streaming setup, allowing me to set up a cosplay streaming area so I can do creative streams. It'll also allow me to purchase a bunch of new \"outfits\" for my lewd kouhais!",
                "reached_at": null,
                "title": ""
            },
            "id": "738718",
            "type": "goal"
        }
    ],
    "links": {
        "self": "https://api.patreon.com/pledges/6131804"
    }
}
`
